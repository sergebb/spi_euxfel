#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import argparse
import config

IMAGE_NAME = 'spi_processing'


def calibration_cmd(container_tag, config_path, detached):
    raw_dir = config.get_raw_dir(config_path)
    work_dir, base_config = os.path.split(config_path)

    cmd = ('docker run -v {0}:{0} -v {1}:{1}:ro -w {0} '.format(work_dir, raw_dir)) \
        + ('-d ' if detached else '-i -t ') \
        + ('{} python /pysrc/calibration/create.py -c {}'.format(container_tag, base_config))

    return cmd


def main():
    parser = argparse.ArgumentParser(description='Run calibration through docker container')
    parser.add_argument("-c", dest='config_file', required=True, help="Config file")
    parser.add_argument('-d', dest='detached', action='store_true', help="Run detached contained")
    parser_args = parser.parse_args()

    config_file = os.path.abspath(parser_args.config_file)
    detached = parser_args.detached

    container_config = config.load_container_config()

    cmd = calibration_cmd(IMAGE_NAME, config_file, detached)

    print(cmd)
    os.system(cmd)


if __name__ == '__main__':
    main()
