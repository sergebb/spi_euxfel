#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Lib for work with SPI configuration file
Author: Sergey Bobkov
"""

from __future__ import print_function
import os
import configparser
from itertools import groupby

CONFIG_FILE = 'config.ini'


def parse_runs_string(runs_string):
    result_runs = []
    for chunk in runs_string.split(','):
        if chunk.strip().isdigit():
            result_runs.append(int(chunk))
        else:
            chunk_parts = chunk.split('-')
            if len(chunk_parts) == 2:
                result_runs.extend(range(int(chunk_parts[0]), int(chunk_parts[1]) + 1))
            else:
                raise ValueError('Unknown run format: ' + chunk)

    return result_runs


def create_runs_string(run_list):
    buf = []

    for _, g in groupby(enumerate(run_list), lambda t: t[0] - t[1]):
        g_runs = list(g)
        if len(g_runs) == 1:
            buf.append(str(g_runs[0][1]))
        if len(g_runs) > 1:
            buf.append(str(g_runs[0][1]) + '-' + str(g_runs[-1][1]))

    return ','.join(buf)


class SpiConfig(object):
    file_sect_name = 'FILE_PATH'
    calib_sect_name = 'CALIB_RUNS'
    hit_sect_name = 'HIT_FILTER'
    photon_sect_name = 'PHOTON_CONVERTER'
    detector_assembly_sect_name = 'DETECTOR_ASSEMBLY'
    sample_sect_name = 'SAMPLES'

    raw_dir_prop = 'raw_directory'
    calib_dir_prop = 'calib_directory'
    hits_dir_prop = 'hits_directory'
    # class_dir_prop = 'classification_directory'
    # emc_dir_prop = 'emc_directory'
    # phase_dir_prop = 'phase_directory'
    log_prop = 'log_file'

    brightest_panel_prop = 'brightest_panel'
    lit_pixels_std_mul_thresh_prop = 'lit_pixels_threshold_std_multi'
    total_cells_prop = 'total_cells'
    cell_filter_prop = 'cell_filter'

    noise_thresh_prop = 'noise_threshold'
    photon_adu_ratio_prop = 'photon_adu_ratio'
    bg_frames_prop = 'bg_estimation_frames'

    geom_file_prop = 'geometry_file'

    h5calib_analog_offset_name = 'analog_offset'
    h5calib_gain_thresh_name = 'gain_threshold'
    h5calib_bad_pix_name = 'bad_pixel'

    h5name_trainID = 'train_id'
    h5name_cellID = 'cell_id'


    def __init__(self, config_file, set_work_dir=False):
        self._data = {}
        self.read_config(config_file)

        if set_work_dir:
            config_dir = os.path.dirname(config_file)
            if len(config_dir):
                os.chdir(config_dir)

    def read_config(self, text_file):
        self._config = configparser.ConfigParser()
        self._config.read(text_file)

    def save_config(self, text_file):
        with open(text_file, 'w') as configfile:
            self._config.write(configfile)

    def _set_option(self, sect, option, value):
        self._config[sect][option] = value

    def _get_option(self, sect, option):
        return self._config[sect][option]

    def get_raw_directory(self):
        return self._get_option(SpiConfig.file_sect_name, SpiConfig.raw_dir_prop)

    def set_raw_directory(self, value):
        self._set_option(SpiConfig.file_sect_name, SpiConfig.raw_dir_prop, value)

    def get_hits_directory(self):
        return self._get_option(SpiConfig.file_sect_name, SpiConfig.hits_dir_prop)

    # def get_class_directory(self):
    #     return self._get_option(SpiConfig.file_sect_name, SpiConfig.class_dir_prop)

    # def get_emc_directory(self):
    #     return self._get_option(SpiConfig.file_sect_name, SpiConfig.emc_dir_prop)

    # def get_phase_directory(self):
    #     return self._get_option(SpiConfig.file_sect_name, SpiConfig.phase_dir_prop)

    def get_log_file(self):
        return self._get_option(SpiConfig.file_sect_name, SpiConfig.log_prop)

    def get_calib_runs(self):
        res = {}
        for k, val in self._config[SpiConfig.calib_sect_name].items():
            runs = parse_runs_string(val)
            res[k] = runs
        return res

    def get_calib_directory(self):
        return self._get_option(SpiConfig.file_sect_name, SpiConfig.calib_dir_prop)

    def get_total_cells(self):
        return int(self._get_option(SpiConfig.hit_sect_name, SpiConfig.total_cells_prop))

    def get_brightest_panel(self):
        return int(self._get_option(SpiConfig.hit_sect_name, SpiConfig.brightest_panel_prop))

    def get_lit_pixels_std_mul_thresh(self):
        return float(self._get_option(SpiConfig.hit_sect_name,
                                      SpiConfig.lit_pixels_std_mul_thresh_prop))

    def get_good_cells(self):
        cell_filter_string = self._get_option(SpiConfig.hit_sect_name, SpiConfig.cell_filter_prop)
        return list(eval(cell_filter_string))

    def get_noise_thresh(self):
        thresh = int(self._get_option(SpiConfig.photon_sect_name, SpiConfig.noise_thresh_prop))
        if thresh > 0:
            return thresh
        return self.get_photon_adu_ratio() // 2

    def get_photon_adu_ratio(self):
        return int(self._get_option(SpiConfig.photon_sect_name, SpiConfig.photon_adu_ratio_prop))

    def get_bg_frames(self):
        return int(self._get_option(SpiConfig.photon_sect_name, SpiConfig.bg_frames_prop))

    def get_geom_filename(self):
        return self._get_option(SpiConfig.detector_assembly_sect_name, SpiConfig.geom_file_prop)

    def get_sample_runs(self):
        res = {}
        for k, val in self._config[SpiConfig.sample_sect_name].items():
            runs = parse_runs_string(val)
            res[k] = runs
        return res
