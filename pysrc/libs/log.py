#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Lib for common log operations
Author: Sergey Bobkov
"""

import sys
import logging


def setup_logging(log_file):
    logging.basicConfig(filename=log_file, level=logging.INFO)


def print(msg):
    sys.stderr.write(msg+'\n')
    sys.stderr.flush()


def debug(msg):
    print(msg)
    logging.debug(msg)


def info(msg):
    print(msg)
    logging.info(msg)


def warning(msg):
    print(msg)
    logging.warning(msg)


def error(msg):
    print(msg)
    logging.error(msg)
