#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Lib for common file and directory routines
Author: Sergey Bobkov
"""

import os
from shutil import copy2
import numpy as np
import h5py


def create_file(filename, rewrite=False):
    """Create empty CXI file

    Keyword arguments:
    filename -- name of file
    rewrite -- rewrite content if file exists
    """
    if not rewrite and os.path.exists(filename):
        raise FileExistsError()

    with h5py.File(filename, 'w') as h5file:
        h5file['cxi_version'] = 160
        h5file.create_group('entry_1')


def save_dataset(filename, path, data, rewrite=False, chunks=None, compression=None):
    """Save dataset to CXI file

    Keyword arguments:
    filename -- name of file
    path -- path to dataset, relative to /entry_1/
    data -- data to save
    rewrite -- rewrite dataset if exists
    """
    if not os.path.exists(filename):
        create_file(filename)

    with h5py.File(filename, 'a') as h5file:
        group = h5file['entry_1']

        group_tree = path.split('/')
        if len(group_tree) > 1:
            for gname in group_tree[:-1]:
                group = group.require_group(gname)

        dset = group_tree[-1]

        if not rewrite and dset in group:
            raise ValueError('Dataset already exists')
        elif rewrite and dset in group:
            del group[dset]

        if data is None:
            data = h5py.Empty('i')

        group.create_dataset(dset, data=data, chunks=chunks, compression=compression)


def read_dataset(filename, path):
    """Return dataset content from CXI file
    Empty datasets returs None

    Keyword arguments:
    filename -- name of file
    path -- path to dataset, relative to /entry_1/
    """
    with h5py.File(filename, 'r') as h5file:
        entry = h5file['entry_1']
        if path in entry:
            dset = entry[path]
        else:
            raise ValueError("Dataset doesn't exist")

        if dset.shape is not None:
            return dset[:]
        else:
            return None


def create_soft_link(filename, path, link, rewrite=False):
    """Create soft link in CXI file

    Keyword arguments:
    filename -- name of file
    path -- path to link, relative to /entry_1/
    link -- link target, relative to /entry_1/
    """
    with h5py.File(filename, 'a') as h5file:
        entry = h5file['entry_1']
        group = entry

        group_tree = path.split('/')
        if len(group_tree) > 1:
            for gname in group_tree[:-1]:
                group = group.require_group(gname)

        dset = group_tree[-1]

        if not rewrite and dset in group:
            raise ValueError('Dataset already exists')
        elif rewrite and dset in group:
            del group[dset]

        if link not in entry:
            raise ValueError('Link target does not exists')

        group[dset] = h5py.SoftLink('/entry_1/' + link)


def get_dataset_shape(filename, path):
    """Return shape of a dataset from CXI file

    Keyword arguments:
    filename -- name of file
    path -- path to dataset, relative to /entry_1/
    """
    with h5py.File(filename, 'r') as h5file:
        entry = h5file['entry_1']
        if path in entry:
            dset = entry[path]
        else:
            raise ValueError("Dataset doesn't exist")

        if not isinstance(dset, h5py.Dataset):
            raise ValueError("Not a dataset: " + path)

        return dset.shape


def get_group_keys(filename, path='/'):
    """Return keys of elements in group from CXI file

    Keyword arguments:
    filename -- name of file
    path -- path to group, relative to /entry_1/
    """
    with h5py.File(filename, 'r') as h5file:
        entry = h5file['entry_1']
        if path == '/':
            group = entry
        elif path in entry:
            group = entry[path]
        else:
            raise ValueError("Dataset doesn't exist")

        if not isinstance(group, h5py.Group):
            raise ValueError("Not a group: " + path)

        return list(group.keys())

def get_total_frames(filename):
    """Return number of images in all entry_1/image_N/data datasets

    Keyword arguments:
    filename -- name of file
    """
    with h5py.File(filename, 'r') as h5file:
        entry = h5file['entry_1']
        num = 0
        for k, group in entry.items():
            if k.startswith('image_'):
                num += len(group['data'])

        return num


def get_sum_data(filename, chunk_size=1000):
    """Return sum of all images in all entry_1/image_N/data datasets

    Keyword arguments:
    filename -- name of file
    chunk_size -- maximum number of images in one chunk, controls speed vs memory consumption
    """
    sum_data = []
    with h5py.File(filename, 'r') as h5file:
        entry = h5file['entry_1']
        for k in entry.keys():
            if k.startswith('image_'):
                sum_chunk = get_sum_dataset(filename, '{}/data'.format(k), chunk_size)
                sum_data.append(sum_chunk)

    if not sum_data:
        return None
    if len(sum_data) == 1:
        return sum_data[0]
    return np.array(sum_data).sum(axis=0)


def get_sum_dataset(filename, path, chunk_size=1000):
    """Get dataset sum along first axis
    Empty datasets returs None

    Keyword arguments:
    filename -- name of file
    path -- path to dataset, relative to /entry_1/
    chunk_size -- maximum number of images in one chunk, controls speed vs memory consumption
    """
    with h5py.File(filename, 'r') as h5file:
        entry = h5file['entry_1']
        if path in entry:
            dset = entry[path]
        else:
            raise ValueError("Dataset doesn't exist")

        if dset.shape is not None:
            sum_dset = np.sum(dset[:chunk_size], axis=0)
            for start in range(chunk_size, len(dset), chunk_size):
                end = start + chunk_size
                sum_dset += np.sum(dset[start:end], axis=0)

            return sum_dset
        else:
            return None


def _copy_part(infile_dataset, result_dataset, infile_filter, result_filter, chunk_size=1000):
    if len(infile_filter) == 1:
        result_dataset[result_filter, ...] = infile_dataset
    else:
        # print(result_filter.shape, result_filter.dtype, result_dataset.shape)
        # print(infile_filter.shape, infile_filter.dtype, infile_dataset.shape)
        # result_dataset[result_filter, ...] = infile_dataset[infile_filter, ...]
        result_filter_sum = np.cumsum(result_filter)
        result_chunk_filter = np.zeros_like(result_filter)
        infile_chunk_filter = np.zeros_like(infile_filter)
        for start in range(0, len(infile_chunk_filter), chunk_size):
            end = start + chunk_size
            result_chunk_filter[:] = 0
            infile_chunk_filter[:] = 0
            infile_chunk_filter[start:end] = infile_filter[start:end]

            if infile_chunk_filter.sum() == 0:
                continue

            if infile_filter[:start].sum() == 0:
                result_start = 0
            else:
                result_start = np.searchsorted(result_filter_sum, infile_filter[:start].sum()) + 1
            result_end = np.searchsorted(result_filter_sum, infile_filter[:end].sum()) + 1
            result_chunk_filter[result_start:result_end] = result_filter[result_start:result_end]

            # print(result_chunk_filter.shape, infile_chunk_filter.shape)
            # print(result_filter.sum(), infile_filter.sum())
            # print(result_chunk_filter.sum(), infile_chunk_filter.sum())
            # print(start, end, result_start, result_end)

            result_dataset[result_chunk_filter, ...] = infile_dataset[infile_chunk_filter, ...]


def _chunk_size(data_chunks, data_shape):
    """Choose appropriate chunk size for result data set,
       test if original chunk size bigger than shape, which is forbidden"""
    if data_chunks is None:
        return None

    for chunk_dim, shape_dim in zip(list(data_chunks), list(data_shape)):
        if chunk_dim > shape_dim:
            return None

    return data_chunks


def _copy_dset(data_group, result_group, dset_name, infile_filter, result_filter, cxi_mode):
    """Copy datasets between open HDF files.
    Function choose best way to copy dataset:
    Constant value, or array with len != number of frames in file -
    copy from first input file, skip others.
    Array with len == number of frames -
    copy as a chunk of big array with len == total frames

    Keyword arguments:
    data_group -- parent group in input HDF file
    result_group -- parent group in result HDF file
    dset_name -- name of input dataset and result dataset to be created
    infile_filter -- selection of frames in input dataset
    result_filter -- mask to put data in result dataset
    """

#     if data_group[dset_name].dtype.kind == 'S':
#         if dset_name not in result_group:
#             result_group[dset_name] = data_group[dset_name][...]
    if cxi_mode and dset_name in ['mask', 'image_center']:
        if dset_name not in result_group:
            result_group[dset_name] = data_group[dset_name][:]
    elif len(data_group[dset_name].shape) == 0:
        if dset_name not in result_group:
            result_group[dset_name] = data_group[dset_name][()]
    elif len(data_group[dset_name]) != len(infile_filter):
        if dset_name not in result_group:
            result_group[dset_name] = data_group[dset_name][...]
    elif len(data_group[dset_name]) == len(infile_filter):# and \
            #data_group[dset_name].shape[1:] == infile_filter.shape[1:]:
        if dset_name not in result_group:
            # Fix for forbidden chunk sizes
            dset_chunks = _chunk_size(data_group[dset_name].chunks, data_group[dset_name].shape)

            result_group.create_dataset(dset_name, \
                                        (len(result_filter),) + data_group[dset_name].shape[1:], \
                                        dtype=data_group[dset_name].dtype,\
                                        chunks=dset_chunks,
                                        compression=data_group[dset_name].compression, \
                                        compression_opts=data_group[dset_name].compression_opts)

        if result_group[dset_name].shape[0] == len(result_filter):
            _copy_part(data_group[dset_name], result_group[dset_name],
                       infile_filter, result_filter)


def copy_group(data_group, result_group, infile_filter, result_filter, cxi_mode):
    """Copy groups between open HDF files.
    Function recreates softlink, call itself recursively for child groups
    and call _copy_dset for datasets

    Keyword arguments:
    data_group -- parent group in input HDF file
    result_group -- parent group in result HDF file
    infile_filter -- selection of frames in input dataset
    result_filter -- mask to put data in result dataset
    """
    for k in data_group.keys():
        key_data = data_group.get(k, getlink=True)
        if isinstance(key_data, h5py.SoftLink):
            if k not in result_group:
                result_group[k] = h5py.SoftLink(key_data.path)
            continue

        key_data = data_group[k]

        if isinstance(key_data, h5py.Group):
            if k not in result_group:
                new_result_group = result_group.create_group(k)
            else:
                new_result_group = result_group[k]
            copy_group(key_data, new_result_group, infile_filter, result_filter, cxi_mode)
        elif isinstance(key_data, h5py.Dataset):
            _copy_dset(data_group, result_group, k, infile_filter, result_filter, cxi_mode)


def _combine_datasets(input_dsets, output_group, key, cxi_mode=False):
    el_shape = None
    for dset in input_dsets:
        if dset.shape is not None:
            if len(dset.shape) == 0:
                output_group[key] = dset[()]
                return
            if cxi_mode and (key in ['mask', 'image_center']):
                output_group[key] = dset[:]
                return
            if len(dset.shape) == 1:
                el_shape = 0
            else:
                el_shape = dset.shape[1:]

    if el_shape is None:
        output_group.create_dataset(key, data=h5py.Empty(input_dsets[0].dtype))
        return

    dset_len = sum(len(dset) for dset in input_dsets if dset.shape is not None)

    if el_shape != 0:
        dset_shape = (dset_len,) + el_shape
    else:
        dset_shape = (dset_len,)

    out_dset = output_group.create_dataset(key, dset_shape,
                                           chunks=input_dsets[0].chunks,
                                           dtype=input_dsets[0].dtype,
                                           compression=input_dsets[0].compression,
                                           compression_opts=input_dsets[0].compression_opts)

    chunk_start = 0
    for dset in input_dsets:
        if dset.shape is None:
            continue
        chunk_len = len(dset)

        out_dset[chunk_start:chunk_start+chunk_len] = dset[:]
        chunk_start += chunk_len


def _combine_groups(input_groups, output_group, cxi_mode=False):
    keys = {k for g in input_groups for k in g.keys()}
    for k in keys:
        for group in input_groups:
            if k in group:
                key_data = group.get(k, getlink=True)
                break

        if isinstance(key_data, h5py.SoftLink):
            output_group[k] = h5py.SoftLink(key_data.path)
            continue

        if cxi_mode and input_groups[0].name == '/entry_1' and \
                k.startswith('image_') and k[6:].isdigit():
            continue

        for group in input_groups:
            if k in group:
                key_data = group.get(k)
                break

        if isinstance(key_data, h5py.Group):
            _combine_groups([group[k] for group in input_groups if k in group],
                            output_group.create_group(k), cxi_mode)
        elif isinstance(key_data, h5py.Dataset):
            _combine_datasets([group[k] for group in input_groups if k in group],
                              output_group, k, cxi_mode)
        else:
            raise ValueError('Element type is not known for dset {}, {}'.format(k, type(key_data)))

    # Process image_n groups separately. Combine groups only with same center and mask values
    if cxi_mode and input_groups[0].name == '/entry_1':
        image_groups = [g[k] for g in input_groups for k in g.keys()
                        if k.startswith('image_') and k[6:].isdigit()]
        n_image = len(image_groups)

        mask_dict = {}
        center_dict = {}
        assembly = np.zeros((n_image,), dtype=int)
        for i, image in enumerate(image_groups):
            if image['mask'].shape is None:
                continue

            center = image['image_center'][:]
            mask = image['mask'][:]

            if not mask_dict:
                key = 1
            else:
                key = max(mask_dict.keys()) + 1
                for k, mval in mask_dict.items():
                    if (mask == mval).all() and (center == center_dict[k]).all():
                        key = k
                        break

            if key not in mask_dict.keys():
                mask_dict[key] = mask
                center_dict[key] = center

            assembly[i] = key

        if (assembly == 0).all():
            image = output_group.create_group('image_1')
            image.create_dataset('data', data=h5py.Empty('i'))
            image.create_dataset('mask', data=h5py.Empty('i'))
            return

        output_image_ids = np.unique(assembly[assembly > 0])

        for image_id in output_image_ids:
            new_k = 'image_{:d}'.format(image_id)
            sel_groups = [g for i, g in enumerate(image_groups) if assembly[i] == image_id]
            _combine_groups(sel_groups, output_group.create_group(new_k), True)


def combine_files(filenames, result_fname, cxi_mode=False):
    """Combine several CXI files into one, combining datasets that
    have matching shapes.

    Keyword arguments:
    filenames -- list of filenames for input CXI files
    result_fname -- filename for resulting CXI file
    cxi_mode -- 'mask' and 'image_center' datasets are treated in special way,
    if they are the same in different files, new dimensions are not created,
    if they differ - image_* groups will not combined, new image group will be created if nesessary.
    """
    if not filenames:
        return

    if len(filenames) == 1:
        copy2(filenames[0], result_fname)
        return

    input_h5files = []
    for fname in filenames:
        input_h5files.append(h5py.File(fname, 'r'))

    output_h5file = h5py.File(result_fname, 'w')

    try:
        _combine_groups(input_h5files, output_h5file, cxi_mode)
    except:
        output_h5file.close()
        for h5file in input_h5files:
            h5file.close()
        raise
