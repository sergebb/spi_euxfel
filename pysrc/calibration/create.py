#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Create calibration data by dark runs
Author: Sergey Bobkov
"""

import os
import sys
import argparse

sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
from calibration import calibration
from libs.config import SpiConfig, CONFIG_FILE
from libs import log


def main():
    parser = argparse.ArgumentParser(description="Create calibration data")
    parser.add_argument("-c", "--config", dest='config_file', default=CONFIG_FILE,
                        help="Config file")
    parser.add_argument("-r", "--run", type=int, nargs='+', help="Calibration run to process")
    parser.add_argument("-p", "--panel", type=int, nargs='+', help="Panels to calibrate")
    parser.add_argument("--no-lock", dest='nolock', action='store_true', help="Do not use lock files")
    args = parser.parse_args()

    calib_runs = args.run
    calib_panels = args.panel
    config_file = args.config_file
    lock = not args.nolock

    if not os.path.isfile(config_file):
        parser.error('Config file {} does not exist'.format(config_file))

    spi_config = SpiConfig(config_file, set_work_dir=True)
    log.setup_logging(spi_config.get_log_file())

    config_calib_runs = calibration.set_proper_calib_keys(spi_config.get_calib_runs())

    if len(config_calib_runs) == 0:
        log.debug('No calibration runs in config')
        return

    if calib_runs is None:
        calib_runs = config_calib_runs.keys()
    else:
        calib_runs = [int(run) for run in calib_runs if int(run) in config_calib_runs.keys()]

    calib_runs = sorted(calib_runs)

    if len(calib_runs) == 0:
        log.debug('No calibration runs were selected')
        return

    for run in calib_runs:
        calibration.create_calib_data(spi_config, run, calib_panels, lock)

    log.debug('Calibration finished')


if __name__ == '__main__':
    main()
