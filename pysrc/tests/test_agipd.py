#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Tests for agipd.py
Author: Sergey Bobkov
"""

import os
import sys
import random
import numpy as np
import h5py

sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
from libs import agipd


def test_get_runs(tmpdir):
    runs = list(range(random.randint(1, 10), random.randint(15, 30)))

    for r in runs:
        tmpdir.mkdir("r{:04d}".format(r))

    reader = agipd.AGIPDReader(tmpdir, raw_data=True)
    assert reader.get_runs() == runs


def test_get_num_series_panels(tmpdir):
    run = random.randint(1, 10)
    n_series = random.randint(1, 10)
    n_panels = random.randint(10, 20)
    run_dir = tmpdir.mkdir("r{:04d}".format(run))

    reader = agipd.AGIPDReader(tmpdir, raw_data=True)

    assert reader.get_num_series_panels(run) == (0, 0)

    for i in range(n_panels):
        for j in range(n_series):
            run_dir.join('RAW-R{:04d}-AGIPD{:02d}-S{:05d}.h5'.format(run, i, j)).write('test')

        assert reader.get_num_series_panels(run) == (n_series, i+1)


def test_get_num_frames(tmpdir):
    run = random.randint(1, 10)
    series = random.randint(1, 10)
    panel = random.randint(10, 20)
    run_dir = tmpdir.mkdir("r{:04d}".format(run))

    num_frames = random.randint(10, 20)

    reader = agipd.AGIPDReader(tmpdir, raw_data=True)
    filename = run_dir.join('RAW-R{:04d}-AGIPD{:02d}-S{:05d}.h5'.format(run, panel, series))

    with h5py.File(filename, 'w') as h5file:
        group = h5file.require_group(reader._data_group_path(panel))
        group.create_dataset('data', (num_frames, 10, 20))

    assert reader.get_num_frames(run, series, panel) == num_frames

def test_read_ids(tmpdir):
    run = random.randint(1, 10)
    series = random.randint(1, 10)
    panel = random.randint(10, 20)
    run_dir = tmpdir.mkdir("r{:04d}".format(run))

    num_frames = random.randint(10, 20)

    reader = agipd.AGIPDReader(tmpdir, raw_data=True)
    filename = run_dir.join('RAW-R{:04d}-AGIPD{:02d}-S{:05d}.h5'.format(run, panel, series))

    train_ids = np.arange(10) + random.randint(1, 10000)
    cell_ids = np.arange(10)
    pulse_ids = np.arange(10)

    with h5py.File(filename, 'w') as h5file:
        group = h5file.require_group(reader._data_group_path(panel))

        group.create_dataset('trainId', data=np.hstack(10*[train_ids[:, np.newaxis]]).flatten())
        group.create_dataset('cellId', data=np.concatenate(10*[cell_ids]))
        group.create_dataset('pulseId', data=np.concatenate(10*[pulse_ids]))

    res = reader.read_ids(run, series, panel)

    assert (res[0] == train_ids).all()
    assert (res[1] == cell_ids).all()
    assert (res[2] == pulse_ids).all()

def test_get_index(tmpdir):
    run = random.randint(1, 10)
    n_series = random.randint(2, 3)
    panel = 0
    run_dir = tmpdir.mkdir("r{:04d}".format(run))

    reader = agipd.AGIPDReader(tmpdir, raw_data=True)

    train_ids = np.arange(10) + random.randint(1, 100)
    cell_ids = np.arange(11)

    train_ids = np.hstack(11*[train_ids[:, np.newaxis]]).flatten()
    cell_ids = np.concatenate(10*[cell_ids])

    for s in range(n_series):
        filename = run_dir.join('RAW-R{:04d}-AGIPD{:02d}-S{:05d}.h5'.format(run, panel, s))

        with h5py.File(filename, 'w') as h5file:
            group = h5file.require_group(reader._data_group_path(panel))

            group.create_dataset('trainId', data=train_ids + s*100)
            group.create_dataset('cellId', data=cell_ids)

    idx = random.randint(0, 9)
    s = random.randint(0, n_series - 1)

    tid = train_ids[idx] + 100*s
    cid = idx % 11

    res = reader.get_index(run, panel, [tid], [cid])

    assert res == ([s], [idx])
    assert res[0][0] == s
    assert res[1][0] == idx
