#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Find hits in data by number of litpixels
Author: Sergey Bobkov
"""

import os
import sys
import argparse
from lockfile import LockFile, AlreadyLocked
import numpy as np

import hitdata

sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
from libs.config import SpiConfig, CONFIG_FILE
from libs import names, agipd, log
from calibration import calibration


def hits_filter(series_adu, noise_thresh, lit_pixels_std_mul):
    """Find hits in data by mean + std*lit_pixels_std_mul rule on number of litpixels
    series_adu -- input data. First dimensions - frame id
    noise_thresh -- ADU threshold for pixel to be 'lit'
    lit_pixels_std_mul -- multiplier for standart deviation

    Return: 1d mask of selected frames
    """
    series_adu = series_adu.reshape((series_adu.shape[0], -1))
    litpix = np.sum(series_adu > noise_thresh, axis=1)
    return litpix > (litpix.mean() + litpix.std()*lit_pixels_std_mul)


def find_hits_ids(spi_config, run, series, panel, calib_run):
    """Find ids of hits for specific series in run
    spi_config -- SpiConfig object
    run -- run rumber
    series -- series number
    panel -- which panel is best to use
    calib_run -- which calibration run to use for data calibration

    Return: (hit_train, hit_cell)
    hit_train - train_ids of hits
    hit_cell - cell_ids of hits
    len(hit_train) = len(hit_cell)
    """
    raw_dir = spi_config.get_raw_directory()
    calib_dir = spi_config.get_calib_directory()
    cell_ids = np.array(spi_config.get_good_cells())
    lit_pixels_std_mul = spi_config.get_lit_pixels_std_mul_thresh()
    noise_threshold = spi_config.get_noise_thresh()

    det_reader = agipd.AGIPDReader(raw_dir, raw_data=True)
    det_reader.set_cell_filter(cell_ids)

    train_ids, cell_ids, _ = det_reader.read_ids(run, series, panel)
    if train_ids.size == 0:
        return train_ids, train_ids

    data = det_reader.read_series(run, series, panel)
    log.debug('Data loaded')

    adu = calibration.calibrate_series(calib_dir, data, panel, calib_run, cell_ids)
    log.debug('Data calibrated')

    assert isinstance(adu, np.ndarray)
    assert adu.ndim == 4

    # For each cell separatelly
    hits = np.zeros(adu.shape[:2], dtype=bool)
    for i in range(adu.shape[1]):
        hits[:, i] = hits_filter(adu[:, i], noise_threshold, lit_pixels_std_mul)

    log.debug('{} hits found'.format(np.sum(hits)))

    cur_trains, cur_cells = np.nonzero(hits)
    hit_train = train_ids[cur_trains]
    hit_cell = cell_ids[cur_cells]

    return hit_train, hit_cell


def create_hits_ids(spi_config, run, panel=None):
    """Process one run in RAW data and save hits ids in cxi format
    spi_config -- SpiConfig object
    run -- run rumber
    panel -- specific panel to use, if None - use config value

    Return: 0 on success, -1 on error
    """
    det_reader = agipd.AGIPDReader(spi_config.get_raw_directory(), raw_data=True)

    calib_run = calibration.get_best_calib_run(spi_config, run)
    if calib_run < 0:
        log.error('No calibration runs in config')
        return -1

    if not calibration.validate_calib_run(spi_config, calib_run):
        log.error('No data for calibration run {}'.format(calib_run))
        return -1

    if panel is None:
        panel = spi_config.get_brightest_panel()

    n_series, n_panels = det_reader.get_num_series_panels(run)
    if n_series == 0:
        log.error('Bad RAW data for run: {}'.format(run))
        return -1

    if panel >= n_panels:
        log.error('Wrong panel number: {} >= {}, run {}'.format(panel, n_panels, run))
        return -1

    if det_reader.get_num_frames(run, 0, panel) == 0:
        log.error('No frames for run: {}'.format(run))
        return -1

    for series in range(n_series):
        hit_file = names.generate_hits_series_filename(spi_config.get_hits_directory(), run, series)
        os.makedirs(os.path.dirname(hit_file), exist_ok=True)

        if os.path.exists(hit_file):
            continue

        lock = LockFile(hit_file)
        try:
            lock.acquire(timeout=0)
            log.debug('Started hit-filtering for run {} series {}'.format(run, series))
            hit_train, hit_cell = find_hits_ids(spi_config, run, series, panel, calib_run)

            if hit_train.size > 0:
                hitdata.save_hits_ids(hit_file, hit_train, hit_cell)
            else:
                hitdata.save_hits_ids(hit_file, None, None)

            log.info('Finished hit-filtering for run {} series {}'.format(run, series))

            lock.release()
        except AlreadyLocked:
            continue
        except:
            lock.release()
            raise

    return 0


def main():
    parser = argparse.ArgumentParser(description="Find hits in SPI data")
    parser.add_argument("-c", "--config", dest='config_file', default=CONFIG_FILE,
                        help="Config file")
    parser.add_argument("-r", "--run", type=int, nargs='+', help="Run to process")
    parser.add_argument("-p", "--panel", type=int, help="Best panel to use")
    args = parser.parse_args()

    config_file = args.config_file
    data_runs = args.run
    panel = args.panel

    if not os.path.isfile(config_file):
        parser.error('Config file {} does not exist'.format(config_file))

    spi_config = SpiConfig(config_file, set_work_dir=True)
    log.setup_logging(spi_config.get_log_file())

    if data_runs is None:
        sample_runs = spi_config.get_sample_runs()
        data_runs = sum(sample_runs.values(), [])

    for run in data_runs:
        create_hits_ids(spi_config, run, panel)


if __name__ == '__main__':
    main()
