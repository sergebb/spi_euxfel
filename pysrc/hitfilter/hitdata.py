#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Lib for common file and directory routines
Author: Sergey Bobkov
"""

import os
import sys
import subprocess
from enum import Enum
from lockfile import LockFile, AlreadyLocked
import h5py
import numpy as np

sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
from libs import cxidata, log
from libs.config import SpiConfig


class HitStatus(Enum):
    EMPTY = -4
    LOCKED = -3
    FOUND_IDS = -2
    SAVED = -1
    READY = 0


def get_hit_status(hit_file, lock=True):
    if not os.path.isfile(hit_file):
        return HitStatus.EMPTY

    if lock and LockFile(hit_file).is_locked():
        return HitStatus.LOCKED

    with h5py.File(hit_file, 'r') as h5hits:
        entry = h5hits['entry_1']

        for key in entry.keys():
            els = key.split('_')
            if len(els) == 2 and els[0] == 'image' and els[1].isdigit():
                image = entry[key]
                if 'data' in image:
                    return HitStatus.READY

        for key in entry.keys():
            els = key.split('_')
            if len(els) == 2 and els[0] == 'instrument' and els[1].isdigit():
                instrument = entry[key]
                if 'detector_0/data' in instrument:
                    return HitStatus.SAVED

                if SpiConfig.h5name_trainID in instrument:
                    return HitStatus.FOUND_IDS

        return HitStatus.EMPTY


def save_hits_ids(hit_file, hit_train, hit_cell):
    if hit_train is None:
        cxidata.save_dataset(hit_file, 'instrument_1/' + SpiConfig.h5name_trainID, None)
        cxidata.save_dataset(hit_file, 'instrument_1/' + SpiConfig.h5name_cellID, None)
        return

    assert hit_train.shape == hit_cell.shape
    for cid in np.unique(hit_cell):
        save_hits_ids_instrument(hit_file, cid,
                                 hit_train[cid == hit_cell],
                                 hit_cell[cid == hit_cell])


def get_instrument_ids(hit_file):
    entry_keys = cxidata.get_group_keys(hit_file)

    ids = []
    for key in entry_keys:
        els = key.split('_')
        if len(els) == 2 and els[0] == 'instrument' and els[1].isdigit():
            ids.append(int(els[1]))

    return sorted(ids)


def save_hits_ids_instrument(hit_file, instrument, hit_train, hit_cell):
    assert hit_train.shape == hit_cell.shape
    cxidata.save_dataset(hit_file,
                         'instrument_{:d}/{}'.format(instrument, SpiConfig.h5name_trainID),
                         hit_train, rewrite=True)
    cxidata.save_dataset(hit_file,
                         'instrument_{:d}/{}'.format(instrument, SpiConfig.h5name_cellID),
                         hit_cell, rewrite=True)


def read_hits_ids_instrument(hit_file, instrument):
    hit_train = cxidata.read_dataset(hit_file,
                                     'instrument_{:d}/{}'.format(instrument,
                                                                 SpiConfig.h5name_trainID))
    hit_cell = cxidata.read_dataset(hit_file,
                                    'instrument_{:d}/{}'.format(instrument,
                                                                SpiConfig.h5name_cellID))

    return hit_train, hit_cell


def save_hits_ids_image(hit_file, image, hit_train, hit_cell):
    assert hit_train.shape == hit_cell.shape
    cxidata.save_dataset(hit_file, 'image_{:d}/{}'.format(image, SpiConfig.h5name_trainID),
                         hit_train, rewrite=True)
    cxidata.save_dataset(hit_file, 'image_{:d}/{}'.format(image, SpiConfig.h5name_cellID),
                         hit_cell, rewrite=True)


def read_hits_ids_image(hit_file, image):
    hit_train = cxidata.read_dataset(hit_file,
                                     'image_{:d}/{}'.format(image, SpiConfig.h5name_trainID))
    hit_cell = cxidata.read_dataset(hit_file,
                                    'image_{:d}/{}'.format(image, SpiConfig.h5name_cellID))

    return hit_train, hit_cell


def save_detector_data(hit_file, instrument, panel, data, mask):
    if data is not None:
        assert data.shape[1:] == mask.shape
    cxidata.save_dataset(hit_file, 'instrument_{}/detector_{}/data'.format(instrument, panel),
                         data, rewrite=True)
    cxidata.save_dataset(hit_file, 'instrument_{}/detector_{}/mask'.format(instrument, panel),
                         mask, rewrite=True)


def read_detector_data(hit_file, instrument, panel):
    return cxidata.read_dataset(hit_file,
                                'instrument_{}/detector_{}/data'.format(instrument, panel))


def read_detector_mask(hit_file, instrument, panel):
    return cxidata.read_dataset(hit_file,
                                'instrument_{}/detector_{}/mask'.format(instrument, panel))


def save_image_data(hit_file, image, data, mask):
    if data is None:
        cxidata.save_dataset(hit_file, 'image_{}/data'.format(image), None)
        cxidata.save_dataset(hit_file, 'image_{}/mask'.format(image), None)
    else:
        assert data.shape[1:] == mask.shape
        cxidata.save_dataset(hit_file, 'image_{}/data'.format(image), data, rewrite=True,
                             chunks=(1,) + mask.shape, compression='lzf')
        cxidata.save_dataset(hit_file, 'image_{}/mask'.format(image), mask, rewrite=True)

    cxidata.create_soft_link(hit_file, 'data_{}'.format(image),
                             'image_{}'.format(image), rewrite=True)


def get_image_ids(hit_file):
    entry_keys = cxidata.get_group_keys(hit_file)

    ids = []
    for key in entry_keys:
        els = key.split('_')
        if len(els) == 2 and els[0] == 'image' and els[1].isdigit():
            ids.append(int(els[1]))

    return sorted(ids)


def save_image_center(hit_file, image_id, image_center):
    assert image_center.shape == (3,)
    cxidata.save_dataset(hit_file, 'image_{}/image_center'.format(image_id),
                         image_center, rewrite=True)


def get_num_frames(hit_file):
    num_hits = 0

    for idx in get_image_ids(hit_file):
        shape = cxidata.get_dataset_shape(hit_file, 'image_{}/data'.format(idx))
        if shape is not None:
            num_hits += shape[0]

    return num_hits


def read_image_center(hit_file, image_id):
    return cxidata.read_dataset(hit_file, 'image_{}/image_center'.format(image_id))


def clear_instrument_data(hit_file):
    hit_tmp_file = hit_file + '.tmp'

    with h5py.File(hit_file, 'a') as h5file:
        entry = h5file.require_group('entry_1')

        for k in entry.keys():
            if k.startswith('instrument_'):
                del entry[k]

    try:
        subprocess.run(["h5repack", hit_file, hit_tmp_file], check=True)
        os.rename(hit_tmp_file, hit_file)
    except subprocess.CalledProcessError as err:
        log.error('Error during h5repack, {}'.format(err.output))
        os.remove(hit_tmp_file)
